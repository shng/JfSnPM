package com.jfsnpm.jfsnpm.plugin.quartz;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.plugin.IPlugin;

public class QuartzPlugin implements IPlugin {
	private static final Logger log = LoggerFactory.getLogger(QuartzPlugin.class);
	private static SchedulerFactory schedulerFactory = new StdSchedulerFactory();
	public QuartzPlugin(){
		
	}
	
	public boolean start() {
		try {
			Scheduler scheduler = schedulerFactory.getScheduler();
			scheduler.start();
			return true;
		} catch (SchedulerException e) {
			log.error("Can't start quartz plugin." + e.getMessage());
			throw new RuntimeException("Can't start quartz plugin.", e);
		}
	}

	public boolean stop() {
		try {
			schedulerFactory.getScheduler().shutdown();
			return true;
		} catch (SchedulerException e) {
			log.error("Can't stop quartz plugin." + e.getMessage());
			throw new RuntimeException("Can't stop quartz plugin.", e);
		}
	}
	
	public static Scheduler getScheduler() throws SchedulerException{
		return schedulerFactory.getScheduler();
	}

}
